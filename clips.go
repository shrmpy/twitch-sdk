package twitchsdk

import (
	"fmt"
	"log"

	"github.com/pkg/errors"
)

func NewClip(client string, token string, channel string) []byte {
	end := fmt.Sprintf(TMPL_CLIPS, channel)
	conn := &connector{
		Token:  token,
		Client: client,
	}
	body, err := conn.post(end)
	if err != nil {
		log.Fatal(errors.Wrap(err, "ERROR new clip"))
	}

	return body
}
