package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"

	sdk "gitlab.com/shrmpy/twitch-sdk"
)

func main() {
	b := readArgs()
	body := sdk.Videos(b.Client, b.Token, b.Channel)

	var resp Bucket
	if err := json.Unmarshal(body, &resp); err != nil {
		panic(err)
	}

	// extract first VOD ID
	if len(resp.Data) > 0 {
		fmt.Printf(IFRAME_TMPL, resp.Data[0].Id)
	}
}

const IFRAME_TMPL = `<iframe
  src="https://player.twitch.tv/?video=%s&muted=true&autoplay=false&parent=news.example.com"
  height="240"
  width="400"
  allowfullscreen="true"></iframe>`

type Bucket struct {
	Data       []Video     `json:"data"`
	Pagination interface{} `json:"pagination"`
}

type Video struct {
	Id       string `json:"id"`
	UserId   string `json:"user_id"`
	UserName string `json:"user_name"`
	Title    string `json:"title"`
	URL      string `json:"url"`

	CreatedAt string `json:"created_at"`
	Viewable  string `json:"viewable"`
	Type      string `json:"type"`
}

type bag struct {
	Token   string
	Client  string
	Channel string
}

func readArgs() *bag {

	client := flag.String("client", "", "App client ID")
	token := flag.String("token", "", "Bearer token")

	channel := flag.String("channel", "76147828", "Channel ID")
	flag.Parse()

	if *client == "" {
		fmt.Println("Missing App client (-client CLIENT_ID)")
		panic("Missing app client ID")
	}
	if *token == "" {
		log.Println("Using environment variable for bearer token")
		ev := os.Getenv("TWITCH_TOKEN")
		if ev == "" {
			panic("Environment variable error for TWITCH_TOKEN")
		}
		token = &ev
	}

	return &bag{
		Token:   *token,
		Client:  *client,
		Channel: *channel,
	}
}
