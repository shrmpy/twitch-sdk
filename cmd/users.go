package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	sdk "gitlab.com/shrmpy/twitch-sdk"
)

func main() {
	b := readArgs()
	body := sdk.Users(b.Client, b.Token, b.Username)

	log.Printf("Svc response: %s\n", body)
}

type bag struct {
	Token    string
	Client   string
	Username string
}

func readArgs() *bag {

	client := flag.String("client", "", "App client ID")
	token := flag.String("token", "", "Bearer token")

	name := flag.String("username", "adafruit", "Channel creator user name")
	flag.Parse()

	if *client == "" {
		fmt.Println("Missing App client (-client CLIENT_ID)")
		panic("Missing app client ID")
	}
	if *token == "" {
		log.Println("Using environment variable for bearer token")
		ev := os.Getenv("TWITCH_TOKEN")
		if ev == "" {
			panic("Environment variable error for TWITCH_TOKEN")
		}
		token = &ev
	}

	return &bag{
		Token:    *token,
		Client:   *client,
		Username: *name,
	}
}
