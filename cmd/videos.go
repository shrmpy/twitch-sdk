package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	sdk "gitlab.com/shrmpy/twitch-sdk"
)

func main() {
	b := readArgs()
	body := sdk.Videos(b.Client, b.Token, b.Channel)

	log.Printf("Svc response: %s\n", body)
}

type bag struct {
	Token   string
	Client  string
	Channel string
}

func readArgs() *bag {

	client := flag.String("client", "", "App client ID")
	token := flag.String("token", "", "Bearer token")

	channel := flag.String("channel", "76147828", "Channel ID")
	flag.Parse()

	if *client == "" {
		fmt.Println("Missing App client (-client CLIENT_ID)")
		panic("Missing app client ID")
	}
	if *token == "" {
		log.Println("Using environment variable for bearer token")
		ev := os.Getenv("TWITCH_TOKEN")
		if ev == "" {
			panic("Environment variable error for TWITCH_TOKEN")
		}
		token = &ev
	}

	return &bag{
		Token:   *token,
		Client:  *client,
		Channel: *channel,
	}
}
