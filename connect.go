package twitchsdk

import (
	"fmt"
	"io"

	"net/http"

	"github.com/pkg/errors"
)

const TMPL_USERS = "https://api.twitch.tv/helix/users?login=%s"
const TMPL_VIDEOS = "https://api.twitch.tv/helix/videos?user_id=%s"
const TMPL_CLIPS = "https://api.twitch.tv/helix/clips?broadcaster_id=%s"
const TMPL_AUTH = "Bearer %s"

type connector struct {
	Token  string
	Client string
}

func (c *connector) post(end string) ([]byte, error) {
	return c.reqMethod("POST", end)
}

func (c *connector) get(end string) ([]byte, error) {
	return c.reqMethod("GET", end)
}

func (c *connector) reqMethod(method string, end string) ([]byte, error) {

	tok := fmt.Sprintf(TMPL_AUTH, c.Token)

	client := &http.Client{}
	req, err := http.NewRequest(method, end, nil)
	if err != nil {
		return []byte{}, errors.Wrap(err, "ERROR creating request")
	}
	// specify the token in the request header
	req.Header.Add("Authorization", tok)
	req.Header.Add("client-id", c.Client)
	resp, err := client.Do(req)
	if err != nil {
		return []byte{}, errors.Wrap(err, "")
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, errors.Wrap(err, "ERROR ReadAll on response")
	}

	//TODO return stream
	return body, nil
}
