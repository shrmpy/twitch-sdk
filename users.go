package twitchsdk

import (
	"fmt"
	"log"

	"github.com/pkg/errors"
)

func Users(client string, token string, username string) []byte {
	end := fmt.Sprintf(TMPL_USERS, username)
	conn := &connector{
		Token:  token,
		Client: client,
	}

	body, err := conn.get(end)
	if err != nil {
		log.Fatal(errors.Wrap(err, "ERROR get users"))
	}

	return body
}
