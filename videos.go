package twitchsdk

import (
	"fmt"
	"log"

	"github.com/pkg/errors"
)

func Videos(client string, token string, channel string) []byte {
	end := fmt.Sprintf(TMPL_VIDEOS, channel)
	// TODO limit to one day, for now
	end = end + "&period=day&first=3"

	conn := &connector{
		Token:  token,
		Client: client,
	}
	body, err := conn.get(end)
	if err != nil {
		log.Fatal(errors.Wrap(err, "ERROR get videos"))
	}

	return body
}
